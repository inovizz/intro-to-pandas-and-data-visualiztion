# Intro to Pandas and Data Visualiztion

Please look into the respective notebooks to check the code used during the session for Pandas & Matplotlib.

- [Pandas](https://gitlab.com/inovizz/intro-to-pandas-and-data-visualiztion/-/blob/master/Introduction%20to%20Pandas%20&%20Data%20Visualization.ipynb)
- [Matplotlib](https://gitlab.com/inovizz/intro-to-pandas-and-data-visualiztion/-/blob/master/Matplotlib%20Tutorial.ipynb)

## Setup Instructions

- First step is to install Python
  - [RealPython has comprehensive steps to Install Python](https://realpython.com/installing-python/)
- Once Python is installed, please use below commands to install some required libraries:

```sh
pip install jupyter
pip install pandas
pip install numpy
```

- Go to the repository folder and run this command to run jupyter-notebook

```sh
cd data-visualization
jupyter-notebook
# After running above command you shall see following output
```

![Output](images/output.png)

- We can then access the notebook on the browser using localhost:8888 URL
- Open the notebook you'd like to access, it shall open up in next tab
- To run any cell in notebook, you can use this command

```sh
Shift+Enter
```

## Some References

- Pandas : <https://pandas.pydata.org/docs/>
- Jupyter Notebook : <https://jupyter-notebook.readthedocs.io/en/stable/>
- Matplotlib : <https://matplotlib.org/stable/contents.html>
- Numpy : <https://numpy.org/doc/>
- Pandas Cheatsheet : <https://github.com/pandas-dev/pandas/blob/master/doc/cheatsheet/Pandas_Cheat_Sheet.pdf>
